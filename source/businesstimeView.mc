using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.Application;
using Toybox.Time.Gregorian;
using Toybox.Time;

class businesstimeView extends WatchUi.WatchFace {

    function initialize() {
        WatchFace.initialize();
    }

    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));
    }

    function onShow() {
    }
    
    function time() {
        var timeFormat = "$1$:$2$";
        var clockTime = System.getClockTime();
        var hours = clockTime.hour;
        if (!System.getDeviceSettings().is24Hour) {
            if (hours > 12) {
                hours = hours - 12;
            }
        } else {
            if (Application.getApp().getProperty("UseMilitaryFormat")) {
                timeFormat = "$1$$2$";
                hours = hours.format("%02d");
            }
        }
        var timeString = Lang.format(timeFormat, [hours, clockTime.min.format("%02d")]);

        var view = View.findDrawableById("TimeLabel");
        view.setColor(Graphics.COLOR_WHITE);
        view.setText(timeString);
    }
    
    function dateView() {
        var dateView = View.findDrawableById("Date");
        dateView.setColor(Graphics.COLOR_WHITE);
        
        var today = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
		var dateString = Lang.format(
		    "$1$, $2$ $3$",
		    [
		        today.day_of_week,
		        today.month,
		        today.day,
		    ]
		);
		
        dateView.setText(dateString);
    }
    
    function calendar() {
        var calTop = View.findDrawableById("CalTop");
        
        calTop.setText("10:15 Sprint Review");
        
        var calBottom = View.findDrawableById("CalBottom");
        
        calBottom.setText("25m Sir Mix-a-lot, +1");
    }

    function onUpdate(dc) {
		time();
		dateView();
		calendar();
        
		View.onUpdate(dc);
    }

    function onHide() {
    }

    function onExitSleep() {
    }

    function onEnterSleep() {
    }

}
