using Toybox.WatchUi;
using Toybox.Application;
using Toybox.Graphics;
using Toybox.System;

class Background extends WatchUi.Drawable {

	var totoro;

    function initialize() {
        var dictionary = {
            :identifier => "Background"
        };

        Drawable.initialize(dictionary);
		totoro = new WatchUi.Bitmap({:rezId=>Rez.Drawables.Totoro,:locX=>0,:locY=>0});
    }

    function draw(dc) {
    	var background = 0x575757;
        dc.setColor(Graphics.COLOR_TRANSPARENT, background);
        dc.clear();
        totoro.draw(dc);
    }
}
